package main

import (
	"flag"
	"time"

	"github.com/awlunsfo/kubeDash/k8s"
	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
)

var namespace string

// CreateTable creates a table
func main() {
	flag.StringVar(&namespace, "namespace", "default", "list pods for the namespace")

	client := k8s.NewClient()

	app := tview.NewApplication()
	table := tview.NewTable()
	table.SetBackgroundColor(tcell.NewRGBColor(47, 40, 51))

	go func(table *tview.Table) {
		for {
			rows := client.GetPods(namespace)
			rowNum := 1
			table.Clear()
			for _, row := range rows {
				for index, cell := range row {
					table.SetCell(rowNum, index, tview.NewTableCell(cell))
				}
				rowNum++
			}

			headers := setHeaders("NAME", "READY", "STATUS")
			for index, cell := range headers {
				table.SetCell(0, index, cell)
			}

			time.Sleep(1 * time.Second)
			app.Draw()
		}

	}(table)

	table.Select(0, 0).SetFixed(1, 1).SetDoneFunc(func(key tcell.Key) {
		if key == tcell.KeyEscape {
			app.Stop()
		}
		if key == tcell.KeyEnter {
			table.SetSelectable(true, true)
		}
	}).SetSelectedFunc(func(row int, column int) {
		table.GetCell(row, column).SetTextColor(tcell.ColorRed)
		table.SetSelectable(false, false)
	})
	if err := app.SetRoot(table, true).Run(); err != nil {
		panic(err)
	}
}

func setHeaders(headers ...string) []*tview.TableCell {
	tableCells := []*tview.TableCell{}

	for _, header := range headers {
		cell := tview.NewTableCell(header).
			SetTextColor(tcell.ColorWhite).
			SetAlign(tview.AlignLeft)
		tableCells = append(tableCells, cell)
	}

	return tableCells
}
