package k8s

import (
	"fmt"
	"strconv"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// GetPods gets pods
func (me *Client) GetPods(namespace string) [][]string {
	pods, _ := me.Client.CoreV1().Pods(namespace).List(metav1.ListOptions{})
	rows := [][]string{}
	for _, pod := range pods.Items {
		ready := readyContainers(pod.Status.ContainerStatuses)
		total := strconv.Itoa(len(pod.Status.ContainerStatuses))
		status := getStatus(pod.Status)

		row := []string{pod.Name,
			fmt.Sprintf("%v/%v", ready, total),
			fmt.Sprintf("%v", status),
		}
		rows = append(rows, row)
	}

	return rows
}

func readyContainers(containers []corev1.ContainerStatus) string {
	count := 0

	for _, container := range containers {
		if container.Ready {
			count++
		}
	}
	return strconv.Itoa(count)
}

func getStatus(status corev1.PodStatus) string {
	if status.Phase == "Failed" {
		return status.Reason
	}

	return fmt.Sprintf("%s", status.Phase)
}
